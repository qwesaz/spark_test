package test;

import java.nio.ByteBuffer;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.streaming.Duration;
import org.apache.spark.streaming.api.java.JavaPairInputDStream;
import org.apache.spark.streaming.api.java.JavaStreamingContext;
import org.apache.spark.streaming.kafka.KafkaUtils;

import com.datastax.driver.core.BoundStatement;
import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.Cluster.Builder;
import com.datastax.driver.core.PreparedStatement;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;
//import com.datastax.spark.connector.cql.CassandraConnector;

import kafka.serializer.StringDecoder;

class CassandraConnector {
	 
    private Cluster cluster;
    private Session session;
    public void connect(String node, Integer port) {
        Builder b = Cluster.builder().addContactPoint(node);
        if (port != null) {
            b.withPort(port);
        }
        cluster = b.build();
 
        session = cluster.connect();
    }

    public Session getSession() {
        return this.session;
    }
 
    public void close() {
        session.close();
        cluster.close();
    }
}

public class Main {
	
	//private static transient Session session;
	private static transient CassandraConnector connector;
	private static transient BoundStatement boundStatement;
	
	public static void main(String[] args) throws Exception {
		SparkConf conf = new SparkConf().setAppName("test").setMaster("local[*]");
		//conf.set("spark.cassandra.connection.host", "localhost");
		//conf.set("spark.cassandra.connection.port", "9042");
		conf.set("spark.ui.port", "4040");
		//conf.set("spark.cassandra.auth.username", "cassandra");
		//conf.set("spark.cassandra.auth.password", "cassandra");
		JavaSparkContext sc = new JavaSparkContext(conf);
		JavaStreamingContext ssc = new JavaStreamingContext(sc, new Duration(2000));
		connector = new CassandraConnector();
		connector.connect("localhost", 9042);
		PreparedStatement ps = connector.getSession().prepare("insert into test.test (id, data) values(?,?)");
		boundStatement = new BoundStatement(ps);
		
		//CassandraConnector connector = CassandraConnector.apply(sc.getConf());
		//session = connector.getSession();

		Set<String> topics = Collections.singleton("test");
		Map<String, String> kafkaParams = new HashMap<String, String>();
		kafkaParams.put("metadata.broker.list", "localhost:9092");

		JavaPairInputDStream<String, String> directKafkaStream = KafkaUtils.createDirectStream(ssc, String.class,
				String.class, StringDecoder.class, StringDecoder.class, kafkaParams, topics);

		directKafkaStream.foreachRDD(rdd -> {
			rdd.foreach(record -> {
				System.out.println(record._2);
				Row row = connector.getSession().execute("SELECT max(id) FROM test.test").one();
				Long maxId = row.getLong(0)+1;
				connector.getSession().execute(boundStatement.bind(maxId, ByteBuffer.wrap(record._2.getBytes())));
			});
		});

		ssc.start();
		ssc.awaitTermination();
		connector.close();

	}
}
